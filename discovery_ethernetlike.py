#!/usr/bin/env python3

# Protocol description: https://bitbucket.org/tlabwest/osdpwg/src/main/discovery_ethernetlike.md

import random
from enum import Enum

import matplotlib.pyplot as plt
import numpy as np


# a higher MS_PER_TICK means collisions are more likely
MS_PER_TICK = 10

TICKS_TO_SEND_MESSAGE = 20 // MS_PER_TICK
TICKS_TO_TIMEOUT = 200 // MS_PER_TICK
TICKS_RANDOM_RANGE_FROM = 20 // MS_PER_TICK
TICKS_RANDOM_RANGE_TO = 150 // MS_PER_TICK


def printx(s):
#    print(s)
    pass


class MessageId(Enum):
    DISCOVER = 1
    DISCOVER_REPLY = 2
    SET_ADDRESS = 3
    ACK = 4
    COLLISION = 5


class Message:
    num_messages_created = 0
    
    def __init__(self, command, cuid=None):
        self.command = command
        self.cuid = cuid
        Message.num_messages_created += 1
        
    def __str__(self):
        return "Message {}, cuid {}".format(self.command, self.cuid)


class PD:
    def __init__(self, cuid):
        self.is_finished = False
        self.cuid = cuid
        self.pending_reply = None
        self.reply_at_tick = None
        
    def process_message(self, message, tick):
        if self.is_finished:
            # this device is done
            return
        
        # discard any pending reply
        self.pending_reply = None
        self.reply_at_tick = None
        
        if message.command == MessageId.DISCOVER:
            # select a random time to wait before sending reply
            self.reply_at_tick = tick + random.randint(TICKS_RANDOM_RANGE_FROM, TICKS_RANDOM_RANGE_TO)
            self.pending_reply = Message(MessageId.DISCOVER_REPLY, self.cuid)
            printx("PD {}: waiting until tick {} to send reply".format(self.cuid, self.reply_at_tick))
        elif message.command == MessageId.SET_ADDRESS:
            if message.cuid == self.cuid:
                printx("PD {}: got an address, done!".format(self.cuid))
                self.reply_at_tick = tick + 1
                self.pending_reply = Message(MessageId.ACK, self.cuid)
                self.is_finished = True
    
    def step_time(self, tick):
        message = None
        
        if self.pending_reply != None and self.reply_at_tick == tick:
            message = self.pending_reply
            self.reply_at_tick = None
        
        return message
    
    def __str__(self):
        return "PD(cuid: {}, finished: {})".format(self.cuid, self.is_finished)
    

class Controller:
    def __init__(self):
        self.is_finished = False
        self.send_next_discover_at_tick = 1
        self.got_reply_to_latest_discover = True
        self.pending_command = None
    
    def process_message(self, message, tick):
        if message.command == MessageId.COLLISION:
            # Two or more PDs responded at the same time, send another DISCOVER message
            self.got_reply_to_latest_discover = True
            self.send_next_discover_at_tick = tick + TICKS_TO_SEND_MESSAGE
        
        if message.command == MessageId.DISCOVER_REPLY:
            # Got a single reply, assign address to PD
            self.got_reply_to_latest_discover = True
            self.pending_command = Message(MessageId.SET_ADDRESS, message.cuid)
            self.send_next_discover_at_tick = tick + TICKS_TO_SEND_MESSAGE
    
    def step_time(self, tick):
        if self.pending_command != None:
            message = self.pending_command
            self.pending_command = None
            return message
        
        if tick >= self.send_next_discover_at_tick:
            if self.got_reply_to_latest_discover == False:
                printx("{}: No devices replied to latest DISCOVER, must be done.".format(tick))
                self.is_finished = True
            else:
                # We had unassigned devices reply to previous DISCOVER, send one more
                self.got_reply_to_latest_discover = False
                self.send_next_discover_at_tick = tick + TICKS_TO_TIMEOUT
                return Message(MessageId.DISCOVER)

    def __str__(self):
        return "ACU(finished: {})".format(self.is_finished)


class Simulator:
    def __init__(self, num_pds):
        self.devices = [Controller()]
        for cuid in range(0, num_pds):
            self.devices.append(PD(cuid))
    
    def run(self):
        tick = 0
        while False in [d.is_finished for d in self.devices] and tick < 100*TICKS_TO_TIMEOUT:
            tick += 1
            messages = []
            # step time, collect messages to process
            #print("Tick {}. Devices: {}".format(tick, [str(d) for d in self.devices]))
            for d in self.devices:
                msg = d.step_time(tick)
                if msg != None:
                    messages.append(msg)
            # more than one message == collision
            if len(messages) > 1:
                printx("COLLISION! {} messages sent at once".format(len(messages)))
                printx([str(m) for m in messages])
                messages = [Message(MessageId.COLLISION)]
            # process message if there is one
            if len(messages) == 1:
                message = messages[0]
                printx("{}: processing message {}".format(tick, message))
                for d in self.devices:
                    d.process_message(message, tick)
        printx("Ticks elapsed: {} ({} ms)".format(tick, tick*MS_PER_TICK))
        printx("Total messages created: {}".format(Message.num_messages_created))
        return tick*MS_PER_TICK, Message.num_messages_created
    

if __name__ == "__main__":
    num_tests_per_device_count = 250
    results_time = {}
    num_pds_stats = []
    max_devices = 32
    for num_pds in range(1, max_devices+1):
        print("Testing with {} devices".format(num_pds))
        trials = []
        for run in range(1, num_tests_per_device_count):
            Message.num_messages_created = 0
            sim = Simulator(num_pds)
            ticks,messages = sim.run()
            trials.append(ticks)
        results_time[num_pds] = trials
        
        stats = (
            np.min(trials),
            np.mean(trials),
            np.median(trials),
            np.max(trials),
            np.std(trials)
        )
        print(stats)
        num_pds_stats.append(stats)
    #print(results_time)
    
    mins = list([x[1] - x[0] for x in num_pds_stats])
    means = list([x[1] for x in num_pds_stats])
    meds = list([x[2] for x in num_pds_stats])
    maxes = list([x[3] - x[1] for x in num_pds_stats])
    stds = list([x[4] for x in num_pds_stats])
    
    plt.errorbar(np.arange(1, max_devices + 1), means, yerr=[mins, maxes], fmt='|', capsize=3, label='Absolute Range')
    plt.errorbar(np.arange(1, max_devices + 1), means, yerr=stds, capsize=3, fmt='|', label='Standard Deviation')
    plt.plot(np.arange(1, max_devices + 1), means, '.-', label='Mean')
    plt.plot(np.arange(1, max_devices + 1), meds, '_', label='Median')
    plt.suptitle('Time Required to Discover PDs, Ethernet-style')
    plt.title('{} Trials per Device Count'.format(num_tests_per_device_count))
    plt.ylabel('Time (ms)')
    plt.xlabel('Number of PDs')
    plt.xlim(0, max_devices + 1)
    plt.legend()
    plt.show()
