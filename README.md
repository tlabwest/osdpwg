# OSDP tests and simulations

Things related to work on the OSDP working group.

[discovery_ethernetlike.py](discovery_ethernetlike.py) is a simulation to see how
long it would take to identify all devices on the bus using an Ethernet-inspired
protocol, where the ACU sends DISCOVER messages that the PDs reply to after a random
delay.

[discovery_ethernetlike.md](discovery_ethernetlike.md) is a spec of the above protocol.

## Preparing the Python environment

We use a few external modules, so we need to prepare that:

`python3 -m venv venv`

`source venv/bin/activate` (or e.g. activate.fish if you use the fish shell)

`pip3 install numpy wheel matplotlib`
