# OSDP Device Discovery

We'd like to have a way to connect several PDs to an OSDP bus and have the ACU
discover them and assign them addresses. It is not possible to use the broadcast
address as-is, since then all PDs would respond to any message sent and their
replies would collide.


## Ethernet-inspired method

This proposed method is inspired by Ethernet's CSMA/CD collision handling method where
devices wait a random time before retrying a transmission.

The method can be explained as follows: the ACU keeps sending `DISCOVER` messages until there
are no replies. If a PD sees a `DISCOVER` message, it waits a random time before sending a reply;
if it sees another reply on the bus before the time has elapsed, it sends no reply. If the ACU
sees a reply, it assigns an address to the replying PD.

A rough simulation is available in [discovery_ethernetlike.py](discovery_ethernetlike.py).
The simulation implements most of the details described in this document for both ACU
and PD.


### Deviations from normal OSDP behavior

* A bit of state that has to do with communication isn't cleared when a message with
sequence 0 is sent (the "have I been assigned an address yet" bit). This bit is only
cleared when a `STARTING DISCOVERY` message is sent, and set when an address is assigned.

* PDs must be able to re-send a reply to a re-sent `DISCOVER SET ADDRESS`, which goes
against normal OSDP where stored replies are to be cleared when sequence 0 is received.


### Device identification

To identify PDs during discovery we use information already found in osdp_PDID:

- Vendor Code (IEEE OUI), 3 bytes
- Model Number
- Version
- Serial Number, 4 bytes

In total 9 bytes.

This identifying information is called "PD identity" in the message descriptions,
in an attempt to keep it separate from PDID and cUID.


### Discovery messages

All the messages are normal OSDP messages, and share the following properties:

- use the same OSDP command, osdp_DISCOVERY (to be assigned; use 0x8F for spec work)
- use CRC16
- use the broadcast address
- use SQN zero

The data of the messages starts with a single byte containing the discovery-specific command.

Due to the likelihood of collisions, it's important to use CRC16 rather than CKSUM.

All osdp_DISCOVERY messages are addressed to the broadcast address. The replies from PDs indicate 
reply status with the MSB of the address byte as usual.

Since different devices send these messages, we cannot use the sequence number; all messages
addressed to the broadcast address must have sequence number 0.

In a deviation from normal OSDP sequence logic, the PD needs to be able to re-send it's
`DISCOVER_ACK` reply if the ACU re-sends a `DISCOVER_SET_ADDRESS` message. This is needed
to avoid a potential edge case where the ACU believes the `DISCOVER_SET_ADDRESS` message
to have failed, while the PD considers it to have been successful. Normally a message with
sequence 0 will cause the PD to discard any stored reply and prevent resends.

Note that the ACU may decide to send another, non-discovery message instead of sending a
`DISCOVER` message. This may be required to keep communication with already-assigned PDs
active, given that OSDP allows at most 8 seconds between messages.


#### STARTING DISCOVERY

Discovery Command number: 0

Sent by ACU.

Additional data: none.

| Offset     | Value   | Meaning                   |
|------------|---------|---------------------------|
| 0          | 0       | STARTING DISCOVERY        |

When a PD receives a `STARTING DISCOVERY` message, it will clear its
"have I been assigned an address" state and thus respond to future
`DISCOVER` messages.


#### DISCOVER

Discovery Command number: 1

Sent by ACU.

Additional data: none.

| Offset     | Value   | Meaning                   |
|------------|---------|---------------------------|
| 0          | 1       | DISCOVER                  |


#### DISCOVER REPLY

Discovery Command number: 2

Sent by PD.

Additional data: PD's current address, PD identity.

| Offset     | Value   | Meaning                   |
|------------|---------|---------------------------|
| 0          | 2       | DISCOVER REPLY            |
| 1          | 0-126   | PD's current address      |
| 2-10       | 0-255   | PD identity               |


#### DISCOVER SET ADDRESS

Discovery Command number: 3

Sent by ACU.

Additional data: address to assign, PD identity.

| Offset     | Value   | Meaning                   |
|------------|---------|---------------------------|
| 0          | 3       | DISCOVER SET ADDRESS      |
| 1          | 0-126   | PD's new address          |
| 2-10       | 0-255   | PD identity               |

NOTE: The assigned address is considered permanent: the PD should store it
for use after a reboot, or after having lost communication with the ACU. If the
address were transient, a rebooted PD (e.g. due to a bug and subsequent watchdog reset)
runs a high risk of causing an address collision. Consider a case where you have multiple
PDs, all with default address 0, on the same bus.

NOTE: It is important that the ACU uses re-send logic to retry the message
if it fails to receive the expected `DISCOVER_ACK` reply.


#### DISCOVER ACK

Discovery Command number: 4

Sent by PD.

Additional data: PD's new address.

| Offset     | Value   | Meaning                   |
|------------|---------|---------------------------|
| 0          | 4       | DISCOVER ACK              |
| 1          | 0-126   | PD's new address          |


### ACU discovery logic

1.  Send `STARTING DISCOVERY`.
2.  If there is any reply (even a single byte), there is a PD on the bus that doesn't support the discovery
    protocol; abort discovery.
3.  Start discovery loop:
4.  Send `DISCOVER` message to broadcast address.
5.  If there is no reply, discovery process has completed.
6.  If there is a collision (indicated by incomplete or invalid message), wait until bus is idle then go to step 4.
6.  If there is a `DISCOVER REPLY` reply with a valid checksum, send `DISCOVERY SET ADDRESS`
    with the PD identity from the reply.
7.  If there is an ACK reply containing the assigned address, the PD has successfully been assigned an address.
8.  Go to step 4.

```
┏━━━━━━━━━━━━━━━━━━┓                                                                  
┃  Send STARTING   ┃                                                                  
┃    DISCOVERY     ┃                                                                  
┗━━━━━━━━━━━━━━━━━━┛                                                                  
          │                                                                           
          │                                                                           
          │                                                                           
          ▼                                                                           
 ┌─────────────────┐                                                                  
 │ Received reply? │─────────────────┐                                                
 └─────────────────┘                 │                                                
          │                          │                                                
          │                          │                                                
          ▼                          ▼                                                
 ┌ ─ ─ ─ ─ ─ ─ ─ ─ ┐       ┏━━━━━━━━━━━━━━━━━━┓                                       
                           ┃  Send DISCOVER   ┃◀───────────────────────┬─────────────┐
 │  Incompatible   │       ┗━━━━━━━━━━━━━━━━━━┛                        │             │
   device present;                   │                                 │             │
 │      abort      │                 │                                 │             │
                                     │                                 │             │
 └ ─ ─ ─ ─ ─ ─ ─ ─ ┘                 ▼                                 │             │
                           ┌──────────────────┐                        │             │
                           │                  │                        │             │
          ┌───── No ───────│Received a reply? │                        │             │
          │                │                  │                        │             │
          │                └──────────────────┘                        │             │
          │                          │                                 │             │
          │                          │ Yes                             │             │
          │                          │                                 │             │
          ▼                          ▼                                 │             │
 ┌ ─ ─ ─ ─ ─ ─ ─ ─ ┐       ┌──────────────────┐                        │             │
                           │                  │                        │             │
 │    Discovery    │       │ Received invalid │             ┌─────────────────────┐  │
      completed            │      reply?      │──── Yes ───▶│ Wait until bus idle │  │
 │                 │       │                  │             └─────────────────────┘  │
  ─ ─ ─ ─ ─ ─ ─ ─ ─        └──────────────────┘                                      │
                                     │                                               │
                                     │ No                                            │
                                     │                                               │
                                     ▼                                               │
                           ┌──────────────────┐                                      │
                           │                  │                                      │
                           │Received DISCOVER │                                      │
                           │      REPLY?      │──── No ───────────┬──────────────────┘
                           │                  │                   │                   
                           └──────────────────┘                   │                   
                                     │                            │                   
                                     │ Yes                        │                   
                                     │                            │                   
                                     ▼                            │                   
                           ┏━━━━━━━━━━━━━━━━━━┓                   │                   
                           ┃                  ┃                   │                   
                           ┃Send DISCOVER SET ┃                   │                   
                           ┃     ADDRESS      ┃                   │                   
                           ┃                  ┃                   │                   
                           ┗━━━━━━━━━━━━━━━━━━┛                   │                   
                                     │                            │                   
                                     │                            │                   
                                     │                            │                   
                                     ▼                            │                   
                           ┌──────────────────┐                   │                   
                           │                  │                   │                   
                           │  Received ACK?   │──── No ───────────┤                   
                           │                  │                   │                   
                           └──────────────────┘                   │                   
                                     │                            │                   
                                     │ Yes                        │                   
                                     │                            │                   
                                     ▼                            │                   
                           ┌──────────────────┐                   │                   
                           │                  │                   │                   
                           │ Device assigned  │───────────────────┘                   
                           │                  │                                       
                           └──────────────────┘                                       
```

IMPORTANT NOTE: If the discovery process takes a long time, PDs may start running into the
"8 second max time between messages to consider communication to be active" limit in the
OSDP specification. To avoid this, the ACU must send a message to each PD that has been 
assigned an address at intervals shorter than 8 seconds.


### PD discovery logic

1.  Receive `STARTING DISCOVERY` message. This indicates that ACU is looking for devices.
    After seeing this message, the PD will respond to `DISCOVER` messages.
    The PD must not send any reply to this message; this indicates that the PD implements
    the discovery protocol.
2.  Receive `DISCOVER` message.
3.  Select a random delay to wait before sending reply. Valid delay values are 10-160 ms.
4.  Wait the selected time. If any byte is seen on the bus during the wait, go to step 2. 
5.  Send `DISCOVER REPLY` message containing PD identity to broadcast address.
6.  Receive message.
7.  If it is a `DISCOVER` message, go to step 3.
8.  If it is not a `DISCOVER SET ADDRESS` message, go to step 2.
9.  If the PD identity in the message doesn't match PD's own PD identity, go to step 2.
10. Assign address from message.
11. Send `DISCOVER ACK` reply.
12. Stop responding to `DISCOVER` messages. (But respond to re-sends of `DISCOVER SET ADDRESS`).

```

┌───────────────────┐                                                               
│                   │                                                               
│ Receive STARTING  │                                                               
│ DISCOVERY message │                                                               
│                   │                                                               
└───────────────────┘                                                               
          │                                                                         
          │                                                                         
          │                                                                         
          │                                                                         
          ▼                                                                         
┌───────────────────┐                                                               
│                   │                                                               
│ Receive DISCOVER  │                                                               
│      message      │◀─────────────┬──────────────────────────────────────────┐     
│                   │              │                                          │     
└───────────────────┘              │              ┌───────────────┐           │     
          │                        │              │               │           │     
          │                        │              │               ▼           │     
          ▼                        │              │     ┌──────────────────┐  │     
┌──────────────────┐               │              │     │                  │  │  No 
│                  │               │              │     │ Is DISCOVER SET  │  │     
│  Select random   │               │              │     │     ADDRESS?     │──┤     
│      delay       │               │              │     │                  │  │     
│                  │               │              │     └──────────────────┘  │     
└──────────────────┘               │              │               │           │     
          │                        │              │               ▼           │     
          │                        │              │     ┌──────────────────┐  │     
          ▼                        │              │     │                  │  │     
 ┌─────────────────┐               │              │     │ Is addressed to  │  │     
 │                 │               │              │     │       me?        │──┘     
 │      Wait       │               │              │     │                  │        
 │                 │               │              │     └──────────────────┘        
 └─────────────────┘               │              │               │                 
          │                        │              │               │                 
          │                        │              │               ▼                 
          ▼                        │              │      ┌─────────────────┐        
┌──────────────────┐               │              │      │                 │        
│                  │               │              │      │   Assign new    │        
│Byte seen on bus? │──────── Yes ──┘              │      │     address     │        
│                  │                              │      │                 │        
└──────────────────┘                              │      └─────────────────┘        
          │                                       │               │                 
          │ No                                    │               │                 
          │                                       │               ▼                 
          ▼                                       │      ┏━━━━━━━━━━━━━━━━━━━┓        
┏━━━━━━━━━━━━━━━━━━┓                              │      ┃                   ┃        
┃                  ┃      ┌──────────────────┐    │      ┃ Send DISCOVER ACK ┃        
┃  Send DISCOVER   ┃      │                  │    │      ┃                   ┃        
┃  REPLY message   ┃─────▶│ Receive message  │────┘      ┗━━━━━━━━━━━━━━━━━━━┛        
┃                  ┃      │                  │                    │                 
┗━━━━━━━━━━━━━━━━━━┛      └──────────────────┘                    │                 
                                                                  ▼                 
                                                         ┌─────────────────┐        
                                                         │                 │        
                                                         │      Done       │        
                                                         │                 │        
                                                         └─────────────────┘        

```


### Test program

A test program is available in the `bin` folder.

It implements both ACU and PD logic for the discovery protocol (plus some other things as well).

Legal note: Permission is given by TLab West AB to working group members to use the program for testing 
related to the discovery protocol, disassembly etc not permitted.

Requirements:

* Linux; built on Ubuntu 18.04 LTS x64
* A suitable serial port, most likely an USB/RS485 adapter
* Access to said serial port, either through sudo or creative use of group memberships or similar


Example commands:

`sudo ./libosdp-posix --bsp-uarts/0/path /dev/ttyUSB0 --device --device_address 2`

Simulates a PD with address 2. Will respond to discovery messages.

`sudo ./libosdp-posix --bsp-uarts/0/path /dev/tty.usbserial-244101 --discover`

Acts as an ACU. First discovers devices, then polls the discovered devices.

`sudo ./libosdp-posix --bsp-uarts/0/path /dev/tty.usbserial-244101 --discover_only`

Acts as an ACU. First discovers devices, then exits.
